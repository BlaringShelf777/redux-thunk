import { ADD_COMMENT } from "./actionTypes"

export const add_comment = (updateUser) => ({
	type: ADD_COMMENT,
	updateUser,
})

import { add_comment } from "./actions"

export const addCommentThunk = (comment) => {
	return (dispatch, getState) => {
		const parsed_comment = comment.trim()

		if (parsed_comment.length) {
			const { user } = getState()

			const updatedUser = {
				...user,
				comments: [...user.comments, parsed_comment],
			}

			dispatch(add_comment(updatedUser))
		}
	}
}

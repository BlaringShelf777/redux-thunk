import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import GlobalStyle from "./styles/global"
import App from "./App"
import store from "./store"

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<App />
			<GlobalStyle />
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
)

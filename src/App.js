import { useDispatch } from "react-redux"
import { addCommentThunk } from "./store/modules/user/thunk"
import { useSelector } from "react-redux"
import styled from "styled-components"
import { useState } from "react"

const Container = styled.div`
	width: 100%;
	height: 100vh;
	display: flex;
	justify-content: center;
	align-items: flex-end;
	background-color: #6292e6;
	font-family: monospace;

	section {
		height: 80vh;
		width: 50vw;
		padding: 2rem;
		background-color: #aebfd7;
		border-radius: 1rem 1rem 0 0;

		p.username {
			font-size: 2rem;
			font-weight: bold;
		}

		div:not(.input) {
			display: flex;
			flex-direction: column;
			gap: 0.5rem;
			margin: 2rem 0;

			p {
				background-color: #6181b8;
				font-size: 1.3rem;
				color: white;
				border-radius: 5px;
				padding: 1rem;
				flex: 1;
				max-width: 550px;
			}
		}

		div.input {
			display: flex;
			gap: 1rem;
			outline: none;

			input,
			button {
				padding: 0.6rem 1rem;
				outline: none;
				font-family: inherit;
				border-radius: 5px;
				border: 0;
			}

			input {
				flex: 1;
			}

			button {
				background-color: #2284c9;
				color: white;
				cursor: pointer;
			}
		}
	}
`

function App() {
	const [input, setInput] = useState("")
	const user = useSelector((store) => store.user)
	const dispatch = useDispatch()

	const handleSubmit = () => {
		dispatch(addCommentThunk(input))
		setInput("")
	}

	return (
		<Container>
			<section>
				<p className="username">{user.name}</p>
				<div>
					{user.comments.map((comment) => (
						<p key={comment}>{comment}</p>
					))}
				</div>
				<div className="input">
					<input
						onChange={(evt) => setInput(evt.target.value)}
						placeholder="Comment..."
						value={input}
					/>
					<button onClick={handleSubmit}>New comment</button>
				</div>
			</section>
		</Container>
	)
}

export default App
